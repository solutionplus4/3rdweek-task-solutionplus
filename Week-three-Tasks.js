const imgdiv = document.getElementById('images');
const container = document.getElementById('container')
imgdiv.style.display = 'inline flex';

let slides = document.querySelectorAll('.slide');
const buttonsdiv = document.getElementById('buttons');
const prev = document.getElementById('prev');
const next = document.getElementById('next');
const auto = document.getElementById('auto');
const random = document.getElementById('random');

let index = 0;
let slideWidth = slides[index].scrollWidth;





prev.onclick = function () {
    index--;
    if (index < 0) {
        index = slides.length - 1; 
        container.scrollLeft = slides[index].offsetLeft;
    } else {
        container.scrollLeft -= slides[index].clientWidth;
    }
};

next.onclick = function () {
    console.log(slides.length);
    index++;
    if (index >= slides.length) {
        index = 1; 
        container.scrollLeft = 0;
    } else {
        container.scrollLeft += slideWidth;
    }
};


auto.onclick = function () {
    let autoInt = setInterval(() => {
        index++;
        if (index >= slides.length + 1) {
            index = 0; 
            container.scrollLeft = 0;
        } else {
            container.scrollLeft += slides[index].clientWidth;
        }
    }, 1000); 
    imgdiv.addEventListener('click', function (event) {
        clearInterval(autoInt);
    });
};


random.onclick = function () {
    let randomInt = setInterval(() => {
        const randomIndex = Math.floor(Math.random() * slides.length);
        container.scrollLeft = slides[randomIndex].offsetLeft;
    }, 1000);

    imgdiv.addEventListener('click', function (event) {
        clearInterval(randomInt);
    });
};


let apiData = fetch('https://jsonplaceholder.typicode.com/posts')
              .then(resp => resp.json())
              .then(data =>{
                data.map(e => 
                    {
                        let apiDiv = document.createElement('div');
                        apiDiv.classList.add('slide');
                        
                        let title = document.createElement('h2');
                        title.textContent = e.title;
                        title.classList.add('title');
                        apiDiv.appendChild(title);
                        
                        apiDiv.appendChild(document.createElement('br'));
                        apiDiv.appendChild(document.createElement('br'));
                        apiDiv.appendChild(document.createElement('br'));

                        let body = document.createElement('p');
                        body.textContent = e.body;
                        body.classList.add('body');
                        apiDiv.append(body);
                        imgdiv.appendChild(apiDiv); 

                        slides = document.querySelectorAll('.slide');
                    });
              })
              .catch(error =>{
                console.error('error fetching the data ', error);
              });

              
              





